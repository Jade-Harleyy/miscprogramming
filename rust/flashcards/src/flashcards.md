# Flashcards, a program intended to be a free and open source alternative to Quizlet

## This program is intended to provide some or all functionality of Quizlet, including but not limited to the following:

* Creating flash card sets

* Importing and exporting flash cards in files written in markdown (like this document) and Quizlet's format

* Generate practice tests to help you study

## Please note that this is a small hobby project, and is not guarenteed to be activly developed or finished
