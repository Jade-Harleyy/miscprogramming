use std::io;
use std::io::Write;
use std::env;
use rand::Rng;

// this is a singleplayer cli app recreating the board game Mastermind.
// you can find information on how it works here: https://en.wikipedia.org/wiki/Mastermind_(board_game)

fn main() {
    env::set_var("RUST_BACKTRACE", "1");
    let mut peg_hit: i32 = 0;
    let mut peg_near: i32 = 0;
    let game_end = false;
    let game_loop = 0;
    // declaring vecs 
    let mut answer_vec = vec![];
    let mut guess_vec = vec![];
    let mut attempted_answers = vec![];

    // pushes randomly generated values to vec to serve as answer
    let mut i = 0;
    while i <= 3 {
        let answer_value = rand::thread_rng().gen_range(1..6);
        answer_vec.push(answer_value);
        i += 1;
    }

    println!("Answer = {:?}", answer_vec); // prints answer for debugging purposes
    
    println!("Put your guess in");
    println!("Format: (1, 2, 3, 4), values 1-6 for each space");
    println!("Press the return key after each number, I'll make it more practical later");

    while game_loop < 10 && game_end == false {
        // has the player input 4 values and writes them to a vec to serve as the guess
        let mut j = 0;
        while j <= 3 {
            let mut input_text = String::new();
            print!(" > ");
            io::stdout().flush().unwrap();
            io::stdin().read_line(&mut input_text).expect("A String");
            let trimmed: i32 = input_text.trim().parse().expect("Not an integer");
            guess_vec.push(trimmed);
            j += 1;
        }

        // TODO: add an option to confirm or redo answer before submitting
        println!("{:?}", guess_vec); // prints out player input, to make sure they see their answer
        
        let mut k = 0; // used to increment the while loop a few lines ahead
        let mut peg_miss = 0; // defines the amount of missed slots
        if answer_vec == guess_vec { // checks if the whole array is correct
            println!("Yipeeee"); // autism
            break; 
        } else { // in case there are discrepencies between the answer and guess
            while k <= 3 { // loops around 4 times so it can check each bit of the vecs
                attempted_answers.push(guess_vec[k]);
                // println!("attempted answers: {:?}", attempted_answers); // for debugging
                if answer_vec[0] == guess_vec[k] || 
                   answer_vec[1] == guess_vec[k] || 
                   answer_vec[2] == guess_vec[k] || 
                   answer_vec[3] == guess_vec[k] {
                    if answer_vec[k] == guess_vec[k] { // comparison of same position on each vec
                        peg_hit += 1; // adds to total amount of correct slots
                    } else if attempted_answers[0] != guess_vec[k] && 
                              attempted_answers[1] != guess_vec[k] && 
                              attempted_answers[2] != guess_vec[k] && 
                              attempted_answers[3] != guess_vec[k] { 
                        peg_near += 1; // adds to the number of slots that were innacurate
                    } else { // in case the number wasn't on the board at all
                        peg_miss += 1; // adds to the number of missed slots
                        while let Some(_) = attempted_answers.pop() {}
                    }
                } else { // in case the number wasn't on the board at all
                    peg_miss += 1; // adds to the number of missed slots
                    while let Some(_) = attempted_answers.pop() {}
                }
                k += 1; // increments the value of k, stepping closer to the end of the loop  
            }
        }
        println!("{} hit", peg_hit);
        println!("{} innacurate", peg_near);
        println!("{} missed", peg_miss);
        while let Some(_) = guess_vec.pop() {}
        peg_hit = 0;
        peg_near = 0;
    }
}
