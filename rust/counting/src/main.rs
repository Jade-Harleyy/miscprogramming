use std::io;
use std::io::Write;

fn main() {
    
    // declaring variable to be counted first
    let mut this_number = 1;

    println!("Count up, starting with 1:");
    

    loop {

        print!(" > ");
        io::stdout().flush().unwrap(); // magical code that stops line breaking idk

        // takes new input as string, then turns it into an integer
        let mut input_buffer: String = String::new(); 
        io::stdin().read_line(&mut input_buffer).expect("Womp");
        let count_number: i32 = input_buffer.trim().parse().expect("Not an integer :(");

        if count_number == this_number { // checks to see if you can count correctly
            this_number += 1; // moves on to the next number
        } else { // in case you can't count
            println!("Streak broken at {}", this_number); // degrading message
            break; // 's your spine
        }

    }
}
