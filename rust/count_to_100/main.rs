fn main() {

    // set's mutable variable x to 0
    let mut x: i32 = 0;
    
    // while loop to print x and add 5 until it reaches 100
    while x <= 100 {
        println!("x = {}", x);
        x = x + 5;
    }
}
