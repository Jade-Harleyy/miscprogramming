use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("Guess da numbew!");

    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        println!("Pwease input your guess (1-100)");
    
        let mut guess = String::new();
    
        io::stdin()
            .read_line(&mut guess)
            .expect("Faiwed to wead wine");

        let guess: u32 = match guess.trim().parse() { 
            Ok(num) => num, 
            Err(_) => continue, 
        };
    
        println!("You guessed: {guess}");

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too Small TwT"),
            Ordering::Greater => println!("Too Big! >.<"),
            Ordering::Equal => { 
                println!("Just right UwU");
                break;
            }
        }
    }
}
