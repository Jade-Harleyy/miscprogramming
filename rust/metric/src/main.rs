use std::io;
use std::io::Write;

#[allow(unused_variables)]
#[allow(unused_assignments)]

fn main() {

    println!("Pick the type:");
    println!("(1) Length");
    println!("(2) Weight");
    println!("(3) Volume");
    println!("(4) Data");

    print!(" > ");
    io::stdout().flush().unwrap();
    let mut type_case = "grarg";
    let mut silly_type = String::new();
    io::stdin().read_line(&mut silly_type).expect("Boowomp");
    let silly_type: i32 = silly_type.trim().parse().expect("invalid input");
    match silly_type {
        1 => type_case = "meter",
        2 => type_case = "gram",
        3 => type_case = "liter",
        4 => type_case = "bytes",
        _ => type_case = "null",
    }
    println!("Pick the unit:");
    println!("
    (y) yocto{}
    (z) zepto{}
    (a) atto{}
    (f) femto{}
    (p) pico{}
    (n) nano{}
    (u) micro{}
    (m) milli{}
    (c) centi{}
    (d) deci{}
    (b) {}
    (D) deca{}
    (H) hecto{}
    (K) kilo{}
    (M) mega{}
    (G) giga{}
    (T) tera{}
    (P) peta{}
    (E) exa{}
    (Z) zetta{}
    (Y) yotta{}", type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, type_case, );
    // println!("(z) zepto{}", type_case);
    // println!("(a) atto{}", type_case);
    // println!("(f) femto{}\n", type_case);
    // println!("(p) pico{}\n", type_case);
    // println!("(n) nano{}\n", type_case);
    // println!("(u) micro{}\n", type_case);
    // println!("(m) milli{}\n", type_case);
    // println!("(c) centi{}\n", type_case);
    // println!("(d) deci{}\n", type_case);
    // println!("(b) {}\n", type_case);
    // println!("(D) deca{}\n", type_case);
    // println!("(H) hecto{}\n", type_case);
    // println!("(K) kilo{}\n", type_case);
    // println!("(M) mega{}\n", type_case);
    // println!("(G) giga{}\n", type_case);
    // println!("(T) tera{}\n", type_case);
    // println!("(P) peta{}\n", type_case);
    // println!("(E) exa{}\n", type_case);
    // println!("(Z) zetta{}\n", type_case);
    // println!("(Y) yotta{}\n", type_case);

    print!(" > ");
    io::stdout().flush().unwrap();
    let unit_case: f32;
    let mut silly_unit = String::new();
    io::stdin().read_line(&mut silly_unit).expect("Boowomp");
    // let silly_unit: i32 = silly_unit.trim().parse().expect("invalid input");
    match silly_unit {
        y => unit_case = 0.000000000000000000000001,
        z => unit_case = 0.000000000000000000001,
        a => unit_case = 0.000000000000000001,
        f => unit_case = 0.000000000000001,
        p => unit_case = 0.000000000001,
        n => unit_case = 0.000000001,
        u => unit_case = 0.000001,
        m => unit_case = 0.001,
        c => unit_case = 0.01,
        d => unit_case = 0.1,
        b => unit_case = 1.0,
        D => unit_case = 10.0,
        H => unit_case = 100.0,
        K => unit_case = 1000.0,
        M => unit_case = 1000000.0,
        G => unit_case = 1000000000.0,
        T => unit_case = 1000000000000.0,
        P => unit_case = 1000000000000000.0,
        E => unit_case = 1000000000000000000.0,
        Z => unit_case = 1000000000000000000000.0,
        Y => unit_case = 1000000000000000000000000.0,
        _ => unit_case = 0.0,
    }
}
