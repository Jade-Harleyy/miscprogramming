use rand::Rng;
use std::io;
use std::io::Write;

fn main() {

    // Prompting the user to input the value for max_num
    println!("Type the max_num value the number can be");
    print!(" > ");
    io::stdout().flush().unwrap(); // keeps the cursor on the same line as the printed text
   
    // Takes that input and converts it to an integer, which is used to define max_num
    let mut input_buffer: String = String::new();
    io::stdin().read_line(&mut input_buffer).expect("Womp");
    let mut max_num = input_buffer.trim().parse().expect("Not an integer :(");
    
    // Lowest number that guessed_num and secret_num can be
    let mut min_num: i32 = 0;

    // Number the algorithm must guess
    let secret_num = rand::thread_rng().gen_range(min_num..=max_num); 

    loop {
        
        // generates the guess
        let guessed_num: i32 = (max_num + min_num) / 2;
        
        // sets max_num = guessed_num if guessed_num is larger than secret_num
        if guessed_num > secret_num {
            println!("{} is too big", guessed_num);
            max_num = guessed_num;
        } 

        // sets min_num = guessed_num if guessed_num is smaller than secret_num
        else if guessed_num < secret_num {
            println!("{} is too small", guessed_num);
            min_num = guessed_num;
        } 

        // breaks the loop, since the algorith did a good job :3
        else {
            println!("{} is just right", guessed_num);
            break;
        }
    }
}
