Hi, these are random programs I'm making while learning to program :3, thanks for stopping by!

# Relevant Projects

## Binary Search Algorithm Guessing Game

This can be found at ```./cpp/bsa_guessing_game/``` and contains the source file 
and linux binary. Windows and Mac users must compile it themselves.
The program does the following:

1. Asks for the maximum value it can use to generate a number for the program to find.
1. Generate a random number between 0 (min) and the defined maximum value (max)
1. Divides the sum of the minimum and maximum value by 2 to generate the guess (guess)
    * If the number is larger than the guess, it sets the minimum to that guess,
    then repeats step 3.
    * If the number is smaller than the guess, it sets the maximum to that guess,
    then repeats step 3.
1. If both the guess and number are equal, the application is closed.

I actually had a lot of fun with this, it's nothing special, but it was a cool learning experience.