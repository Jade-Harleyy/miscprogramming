#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;


int main () {
    srand (time(NULL));
    //declaring variables
    int guess;
    int number;

    number = rand() % 100 + 1;

    while (true) {
        cout << "Type a number from 1-10.\n";
        cin >> guess;
        if (guess == number) {
            cout << "Just right UwU\n";
            break;
        } else if ( guess > number ) {
            cout << "Too big >~<\n";
            continue;
        } else {
            cout << "Too small :/\n";
            continue;
        }
    }
}
