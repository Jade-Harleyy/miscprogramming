#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;


int main () {
    srand (time(NULL));
    int max;
    int min = 0;
    cout << "Type the max value the number can be\n > ";
    cin >> max;
    int guess;
    int number;
    number = rand() % max + min;
    cout << "The number is " << number << "\n";

    while (true) {
        guess = (max + min) / 2;
        if (guess == number) {
            cout << guess << " is just right\n";
            break;
        } else if ( guess > number ) {
            cout << guess << " is too big\n";
            max = guess;
            continue;
        } else {
            cout << guess << " is too small\n";
            min = guess;
            continue;
        }
    }
}
